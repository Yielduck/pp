#include <pp/pick_type.h>
#include <pp/find_type.h>
#include <pp/pick_property.h>
#include <pp/find_property.h>
#include <gtest/gtest.h>

using S =   signed int;
using U = unsigned int;

template<typename T>
struct IsSignedInt
{
    static constexpr bool value = pp::is_same<T, S>::value;
};

TEST(pick_type, picks)
{
    ASSERT_TRUE((pp::is_same<S, pp::pick_type<0u>::from<S>>::value));
    ASSERT_TRUE((pp::is_same<S, pp::pick_type<0u>::from<S, S>>::value));
    ASSERT_TRUE((pp::is_same<S, pp::pick_type<1u>::from<U, S>>::value));
    ASSERT_TRUE((pp::is_same<S, pp::pick_type<1u>::from<U, S, U>>::value));
    ASSERT_TRUE((pp::is_same<S, pp::pick_type<1u>::from<U, S, U, S>>::value));
    ASSERT_TRUE((pp::is_same<S, pp::pick_type<1u>::from<U, S, U, S, U>>::value));
    ASSERT_TRUE((pp::is_same<S, pp::pick_type<2u>::from<U, U, S, S, U>>::value));
    ASSERT_TRUE((pp::is_same<S, pp::pick_type<3u>::from<U, U, U, S, U>>::value));
}
TEST(pick_type, not_picked)
{
    ASSERT_TRUE((pp::is_same<pp::not_picked, pp::pick_type<0u>::from<>>::value));
    ASSERT_TRUE((pp::is_same<pp::not_picked, pp::pick_type<1u>::from<U>>::value));
    ASSERT_TRUE((pp::is_same<pp::not_picked, pp::pick_type<2u>::from<U, U>>::value));
    ASSERT_TRUE((pp::is_same<pp::not_picked, pp::pick_type<3u>::from<U, U, U>>::value));
    ASSERT_TRUE((pp::is_same<pp::not_picked, pp::pick_type<1024u>::from<U, U, U>>::value));
}
TEST(pick_type, large_list)
{
    ASSERT_TRUE((pp::is_same<S, pp::pick_type<256u>::from
    <
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        S,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U
    >>::value));
}
TEST(find_type, finds)
{
    ASSERT_TRUE((0u == pp::find_type<S>::in<S>));
    ASSERT_TRUE((0u == pp::find_type<S>::in<S, S>));
    ASSERT_TRUE((1u == pp::find_type<S>::in<U, S>));
    ASSERT_TRUE((1u == pp::find_type<S>::in<U, S, U>));
    ASSERT_TRUE((1u == pp::find_type<S>::in<U, S, U, S>));
    ASSERT_TRUE((1u == pp::find_type<S>::in<U, S, U, S, U>));
    ASSERT_TRUE((2u == pp::find_type<S>::in<U, U, S, S, U>));
    ASSERT_TRUE((3u == pp::find_type<S>::in<U, U, U, S, U>));
}
TEST(find_type, not_found)
{
    ASSERT_TRUE((pp::not_found == pp::find_type<S>::in<>));
    ASSERT_TRUE((pp::not_found == pp::find_type<S>::in<U>));
    ASSERT_TRUE((pp::not_found == pp::find_type<S>::in<U, U>));
    ASSERT_TRUE((pp::not_found == pp::find_type<S>::in<U, U, U>));
}
TEST(find_type, large_list)
{
    ASSERT_TRUE((256u == pp::find_type<S>::in
    <
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        S,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        S
    >));
}
TEST(pick_property, picks)
{
    ASSERT_TRUE((pp::is_same<S, pp::pick_property<IsSignedInt>::from<S>>::value));
    ASSERT_TRUE((pp::is_same<S, pp::pick_property<IsSignedInt>::from<S, S>>::value));
    ASSERT_TRUE((pp::is_same<S, pp::pick_property<IsSignedInt>::from<U, S>>::value));
    ASSERT_TRUE((pp::is_same<S, pp::pick_property<IsSignedInt>::from<U, S, U>>::value));
    ASSERT_TRUE((pp::is_same<S, pp::pick_property<IsSignedInt>::from<U, S, U, S>>::value));
    ASSERT_TRUE((pp::is_same<S, pp::pick_property<IsSignedInt>::from<U, S, U, S, U>>::value));
    ASSERT_TRUE((pp::is_same<S, pp::pick_property<IsSignedInt>::from<U, U, S, S, U>>::value));
    ASSERT_TRUE((pp::is_same<S, pp::pick_property<IsSignedInt>::from<U, U, U, S, U>>::value));
}
TEST(pick_property, not_picked)
{
    ASSERT_TRUE((pp::is_same<pp::not_picked, pp::pick_property<IsSignedInt>::from<>>::value));
    ASSERT_TRUE((pp::is_same<pp::not_picked, pp::pick_property<IsSignedInt>::from<U>>::value));
    ASSERT_TRUE((pp::is_same<pp::not_picked, pp::pick_property<IsSignedInt>::from<U, U>>::value));
    ASSERT_TRUE((pp::is_same<pp::not_picked, pp::pick_property<IsSignedInt>::from<U, U, U>>::value));
}
TEST(pick_property, large_list)
{
    ASSERT_TRUE((pp::is_same<S, pp::pick_property<IsSignedInt>::from
    <
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        S,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U
    >>::value));
}
TEST(find_property, finds)
{
    ASSERT_TRUE((0u == pp::find_property<IsSignedInt>::in<S>));
    ASSERT_TRUE((0u == pp::find_property<IsSignedInt>::in<S, S>));
    ASSERT_TRUE((1u == pp::find_property<IsSignedInt>::in<U, S>));
    ASSERT_TRUE((1u == pp::find_property<IsSignedInt>::in<U, S, U>));
    ASSERT_TRUE((1u == pp::find_property<IsSignedInt>::in<U, S, U, S>));
    ASSERT_TRUE((1u == pp::find_property<IsSignedInt>::in<U, S, U, S, U>));
    ASSERT_TRUE((2u == pp::find_property<IsSignedInt>::in<U, U, S, S, U>));
    ASSERT_TRUE((3u == pp::find_property<IsSignedInt>::in<U, U, U, S, U>));
}
TEST(find_property, not_found)
{
    ASSERT_TRUE((pp::not_found == pp::find_property<IsSignedInt>::in<>));
    ASSERT_TRUE((pp::not_found == pp::find_property<IsSignedInt>::in<U>));
    ASSERT_TRUE((pp::not_found == pp::find_property<IsSignedInt>::in<U, U>));
    ASSERT_TRUE((pp::not_found == pp::find_property<IsSignedInt>::in<U, U, U>));
}
TEST(find_property, large_list)
{
    ASSERT_TRUE((256u == pp::find_property<IsSignedInt>::in
    <
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        S,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        U, U, U, U, U, U, U, U, U, U, U, U, U, U, U, U,
        S
    >));
}

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
