This tiny library provides the means for searching in template **parameter packs**.

Inside the namespace `pp`, the library defines
- four helpers: `pick_type`, `find_type`; `pick_property`, `find_property`
- unsigned integral alias `type_index`
- `static constexpr type_index not_found = ...`
- type `not_picked`

To understand how these helpers help, consider the type list `Ts...`, type `T`, and unsigned integer `I`.

If `I` happens to be an index of `T` in `Ts...`, `T` may be picked from `Ts...` using the `pick_type` helper:

```c++
using T = typename pp::pick_type<I>::template from<Ts...>;
```

In case `I >= sizeof...(Ts)`, there is no type to be picked. The effect is that `std::is_same<T, pp::not_picked>` evaluates to true.

The inverse action is possible too: given the type, its index may be found:

```c++
constexpr pp::type_index I = pp::find_type<T>::template in<Ts...>;
```

In case `T` can not be found in `Ts...`, `I == pp::not_found` is true.

In case `T` is not unique in `Ts...`, the least index is returned:

```c++
constexpr pp::type_index I = pp::find_type<T>::in<U, T, U, T, U>; // I == 1u
```

For the other two helpers, consider the type property `Property : Type -> Bool`:

```c++
template<typename T>
struct Property
{
    static constexpr bool value = ...;
};
```

With their help, `Property` may be used for the search instead of `I` or `T`:

```c++
using T = typename pp::pick_property<Property>::template from<Ts...>;
constexpr pp::type_index I = pp::find_property<Property>::template in<Ts...>;
```
