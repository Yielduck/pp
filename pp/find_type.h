#pragma once
#include "is_same.h"
#include "type_index.h"
namespace pp
{

namespace detail
{

template<pp::type_index i, typename T>
constexpr pp::type_index find_type_impl() noexcept
{
    return pp::not_found;
}
template<pp::type_index i, typename T, typename U0, typename... U>
constexpr pp::type_index find_type_impl() noexcept
{
    return pp::is_same<T, U0>::value
        ? i
        : pp::detail::find_type_impl<i + 1, T, U...>();
}

} // namespace detail

template<typename T>
struct find_type
{
    template<typename... U>
    static constexpr pp::type_index in = pp::detail::find_type_impl<0, T, U...>();
};

} // namespace pp
