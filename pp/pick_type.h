#pragma once
#include "enable_if.h"
#include "type_index.h"
namespace pp
{

namespace detail
{

template<pp::type_index I, pp::type_index I0, typename U0, typename... U>
auto pick_type_impl(  signed int)
    -> typename pp::enable_if<I == I0, U0>::type;
template<pp::type_index I, pp::type_index I0>
auto pick_type_impl(unsigned int)
    -> pp::not_picked;
template<pp::type_index I, pp::type_index I0, typename U0, typename... U>
auto pick_type_impl(unsigned int)
{
    return pp::detail::pick_type_impl<I + 1, I0, U...>(0);
}

} // namespace detail

template<pp::type_index I>
struct pick_type
{
    template<typename... U>
    using from = decltype(pp::detail::pick_type_impl<0, I, U...>(0));
};

} // namespace pp
