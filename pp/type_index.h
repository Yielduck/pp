#pragma once
namespace pp
{

using type_index = unsigned int;

static constexpr pp::type_index not_found = static_cast<pp::type_index>(-1);

struct not_picked
{};

} // namespace pp
