#pragma once
namespace pp
{

template<bool, typename>
struct enable_if
{};
template<typename T>
struct enable_if<true, T>
{
    using type = T;
};

} // namespace pp
