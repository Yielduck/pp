#pragma once
#include "is_same.h"
#include "type_index.h"
namespace pp
{

namespace detail
{

template<pp::type_index i, template<typename> class Property>
constexpr pp::type_index find_property_impl() noexcept
{
    return pp::not_found;
}
template<pp::type_index i, template<typename> class Property, typename U0, typename... U>
constexpr pp::type_index find_property_impl() noexcept
{
    return Property<U0>::value
        ? i
        : pp::detail::find_property_impl<i + 1, Property, U...>();
}

} // namespace detail

template<template<typename> class Property>
struct find_property
{
    template<typename... U>
    static constexpr pp::type_index in = pp::detail::find_property_impl<0, Property, U...>();
};

} // namespace pp
