#pragma once
#include "enable_if.h"
#include "type_index.h"
namespace pp
{

namespace detail
{

template<template<typename> class Property, typename U0, typename... U>
auto pick_property_impl(  signed int)
    -> typename pp::enable_if<Property<U0>::value, U0>::type;
template<template<typename> class Property>
auto pick_property_impl(unsigned int)
    -> pp::not_picked;
template<template<typename> class Property, typename U0, typename... U>
auto pick_property_impl(unsigned int)
{
    return pp::detail::pick_property_impl<Property, U...>(0);
}

} // namespace detail

template<template<typename> class Property>
struct pick_property
{
    template<typename... U>
    using from = decltype(pp::detail::pick_property_impl<Property, U...>(0));
};

} // namespace pp
