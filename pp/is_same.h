#pragma once
namespace pp
{

template<typename T, typename U>
struct is_same
{
    static constexpr bool value = false;
};
template<typename T>
struct is_same<T, T>
{
    static constexpr bool value = true;
};

template<typename T, typename U>
constexpr bool is_same<T, U>::value;
template<typename T>
constexpr bool is_same<T, T>::value;

} // namespace pp
